import datetime
import pony.orm as pny

database = pny.Database("sqlite", "databasemark0.sqlite", create_db=True)

class Person(database.Entity):
    """
    Pony ORM of the Person table
    """
    name = pny.Required(str)
    permissions = pny.Optional("Permission") #this is a one way foreign key. Which means every permission mapps to a single person

class Permission(database.Entity):
    """
    Pony ORM model of the album table
    """
    person = pny.Required("Person")#Cross database referencing.
    created_at = pny.Required(datetime.date)
    updated_at = pny.Required(datetime.date)
    isAdmin = pny.Required(bool)
    isAllowed = pny.Required(bool)

pny.sql_debug(True)

database.generate_mapping(create_tables=True)

@pny.db_session
def add_data():
    """"""
    p1 = Person(name="Dane")
    p2 = Person(name = "Peter")
    
    permissions = [{
        "person": p1,
        "created_at": datetime.date(1990, 10, 10),
        "updated_at": datetime.date(1990, 11, 10),
        "isAdmin": True,
        "isAllowed": True
    },

    {
        "person": p2,
        "created_at": datetime.date(2020,2,20),
        "updated_at": datetime.date(2020,3,20),
        "isAdmin": False,
        "isAllowed": True
    }]

    for permission in permissions:
        p = Permission(**permission) #expand person object so that every single key is put into a key value pair


# map the models to the database
# and create the tables, if they don't exist

#Create Primary key for us automatically
#To create foreign key, we pass the model class in to the other class

#https: // stackoverflow.com/questions/19877306/nameerror-global-name-unicode-is-not-defined-in-python-3
#https://docs.ponyorm.org/relationships.html

if __name__ == "__main__":      #Only run this if not being imported as a  module
    add_data()      