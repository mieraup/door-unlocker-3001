const express = require('express');

/**
 * Responsible for providing a public API to access resources
 */
class Server { 

    constructor() { 
        this._server = express();
        this._server.use(express.json());
    }

    /**
     * @opts key, value pair that can contain port: integer 
     */
    start(opts) {
        let port = 4000; //Default port
        if (opts && opts.port && Number.isInteger(opts.port)) { 
            port = opts.port;
        }

        this._setupIndexListen();
        this._server.listen(port);
    }

    _setupIndexListen() {   //Make multiple of these for all requests
        this._server.get('/', (req, res) => { 
            res.send('Hello Dane');
        })
    }
            

    /**
     * Stops the server, releasing any resources aquired
     */
    stop() {
        this._server.close();
    }
}

module.exports = {
    Server
}