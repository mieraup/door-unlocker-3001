const express = require('express');
const fs = require('fs');
const { info } = require('console');

const jsonPath = '../friends.json';
const PORT = 4000;
const ADMIN = 0;
let friends;

const server = express();
server.use(express.json());
readData()



function readData() {
    friends = JSON.parse(fs.readFileSync(jsonPath));
    console.log(friends);
    friends = reorder(friends);
}

server.get('/:id/', getPermission);
function getPermission(req, res) {
    readData(); //This is only here temp
    let data = req.params;
    let user = findPerson(data.id);
    let status = 404;
    let result;

    if(user != -1) {
        status = 200;
        result = friends[user].isAllowed.toString();

        if(friends[user].id == ADMIN) {
            result = result.concat(readFriends());
        }
    }
    
    res.status(status).send(JSON.stringify(result)) //transforms object into a string
}

function findPerson(user) {
    let result = -1;

    for(let i = 0; i< friends.length; i++) {
        if(friends[i].id == user) {
            result = i;
        }
    }
    
    return result;
}

function readFriends() {
    let result = "";

    for(let i = 0; i < friends.length; i++) {
           result = result.concat("," + friends[i].name + " " + friends[i].isAllowed + " " + friends[i].id);  
    }

    return result;
}


server.put('/:id/update/', update); //pulls up json txt editor and modify it on app
function update(req, res) {
    let data = req.params;
    let user = findPerson(data.id);
    let status //= ;

    if(user == ADMIN){
        status = 0;
        fs.writeFile(jsonPath, reorder(req.body)); //req.body must be a string
    }

    res.status(status).redirect('/:id/');
}

function reorder(people) {
    let temp;
    let buffer = 0;

    //quicksort
    for(let i = 0; i < people.length; i++) {
        if(people[i].isAllowed) {
            if(buffer != i) {
                temp = people[buffer];
                people[buffer] = people[i];
                people[i] = temp;
            }
            
            buffer++;
        }
    }
    console.log(people)
    //REady
    // I'm going to show you how to do a database system

    // Persistence
    // Updatable
    // Rollbackable
    // Consistence
    people = selectionSort(0, buffer, people);
    
    people = selectionSort(buffer, people.length, people);
    return people;
} //Garbage

function selectionSort(start, end, array) {
    for(let i = start; i < end; i++) {
        let nextPerson = alphabetize(i, end, array);
        let temp = array[i];
        array[i] = array[nextPerson];
        array[nextPerson] = temp;
    }

    return array;
}

function alphabetize(start, end, array) {
    let result = start;

    for(let i = start + 1; i < end; i++) {
        if(array[result].name.localeCompare(array[i]).name == 1) {  //is smaller
            result = i;
        } 
    }

    return result;
}


server.listen(PORT)

//https://developer.mozilla.org/en-US/docs/Learn/Server-side/Express_Nodejs/routes
//framework express mocha plug in testing project <- responds to response
//get (send), post (written to support), put (replaces orig), delete, patch (includes set of instructions to fix given update is smaller)

// Figure out the flow

// Startup
    // Check database consistence
    // Read config file
// Serving
    // Process request
// Shutdown
  // Gracefully close all active connections
  // Commit cache to database
  // Flush Logs
  // Check database consistency

server.start({port: whatever, config:"path to config"})
server.stop()