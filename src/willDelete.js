let {Pool} = require('pg');

/**
 * Connects and manages connections to the postgreSQL server managing permissions
 */

 class postgresDB {

    constructor(user, host, database, password, port) {
        
    }

    start() {
        //Setup different options like in server.js
        console.log("database is connected");
    }

    stop() {
        
    }

    getUserPermission(user) {
        this._dbPool.query('SELECT isAllowed FROM %s', user);
    }

    isAdmin(user) {
        this._dbPool.query('SELECT isAdmin FROM %s', user);
    }


    //Admin specific operations
    getEveryonesPermissions() {
        
    }

    addUser(ID, name) {

    }

    deleteUser(user) {

    }

    modifyAccess(user) {

    }
}

module.exports = {
    postgresDB
}