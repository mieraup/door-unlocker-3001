import pony.orm as pny
import datetime

class PostgresDB:
    db = pny.Database() #Cannot access outter info from inner classes, entities need to be defined to db

    # Information stored regarding each person in the Database
    class Friend(db.Entity):  # This has to be the db thing
        #ID = pny.PrimaryKey(str)
        name = pny.Required(str)
        permission = pny.Optional("Permission")

    class Permission(db.Entity):
        friend = pny.Required("Friend")
        updated_at = pny.Required(datetime.date)
        isAdmin = pny.Required(bool)
        isAllowed = pny.Required(bool)


    def start(self):  # include different options here
        self.db.bind(provider='postgres', user='postgres', password='dane', host='localhost', database='postgres')
        self.db.generate_mapping(create_tables=True)

    def stop(self):
        self.db.disconnect()

    @pny.db_session
    def add_data():
        p1 = Friend(name="Dane")
        p2 = Friend(name = "Peter")
    
        permissions = [{
            "person": p1,
            "created_at": datetime.date(1990, 10, 10),
            "updated_at": datetime.date(1990, 11, 10),
            "isAdmin": True,
            "isAllowed": True
        },
        {
            "person": p2,
            "created_at": datetime.date(2020,2,20),
            "updated_at": datetime.date(2020,3,20),
            "isAdmin": False,
            "isAllowed": True
        }]

        for permission in permissions:
            p = Permission(**permission)


    # User operations
    @pny.db_session
    def getUserPermission(self, user):
        return self.db.get("select isAllowed from self.db where ID = $user")

    @pny.db_session
    def isAdmin(self, user):
        return self.db.get("select isAdmin from self.db where ID = $user")

    # Admin specific operations
    @pny.db_session
    def getEveryonesPermissions(self):
        return 0#self.db.select(NULL)c for c in self.Friend).order_by(self.Friend.isAllowed, self.Friend.name)

    @pny.db_session
    def addUser(self, newID, newName):
        newPerson = Friend(newId, newName)

    @pny.db_session
    def deleteUser(self, user):
        delete = self.db.get("select ID from self.db where ID = $user")
        delete.delete()

    @pny.db_session
    def modifyAccess(self, user):
        modify = self.db.get(
            "select permission from friends where ID = $user")
        print(modify.isAllowed)

#        http://www.blog.pythonlibrary.org/2014/07/21/python-101-an-intro-to-pony-orm/
